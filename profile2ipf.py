import sys
import logging
# config input and checking
import toml
from pathlib import Path
from schema import Schema, Optional, And, Use
# spatial processing
import geopandas as gpd
import shapely
import numpy as np
# image processing
import imageio
import colour
import skimage
from scipy import ndimage
# output
import csv
import pandas as pd
import imod


def _to_path(s):
    """Change string into path, check if exists"""
    p = Path(s)
    if not p.exists():
        raise FileNotFoundError('File "{}" does not exist'.format(p))
    return s

 
CONFIG_SCHEMA = {
    "bitmap": And(str, Use(_to_path)),
    "shapefile": And(str, Use(_to_path)),
    "name": str,
    "orientation": str,
    Optional("maximum_color_distance", default=0.0): float,
    "spacing": float,
    "colors": dict,
    "elevation_lower": float,
    "elevation_upper": float,
    Optional("pause", default=False): bool,
}


def read_toml(path, schema_dict):
    schema = Schema([schema_dict])
    d = toml.load(path)
    try:
        config = schema.validate([d])
        return config[0]
    except Exception as e:
        sys.exit(e.args[0].split("\n")[-1])

        
# Spatial ---------------------------------------------------------------------

def _reorient(lithogrid, coords, orientation):
    first_x = coords[0, 0]
    first_y = coords[0, 1]
    last_x = coords[-1, 0]
    last_y = coords[-1, 1]
    
    # flip around the image if it doesn't run from West to East
    # or from North to South
    if orientation.lower() == "west-east":
        pass
    elif orientation.lower() == "north-south":
        pass
    elif orientation.lower() == "east-west":
        logging.info("Flipping image to get West-East orientation.")
        lithogrid = lithogrid[:, ::-1]
        orientation = "west-east"
    elif orientation.lower() == "south-north":
        logging.info("Flipping image to get North-South orientation.")
        lithogrid = lithogrid[:, ::-1]
        orientation = "north-south"
    else:
        raise ValueError(
            'Invalid orientation.'
        )
    
    # flip around the shapefile profile if it doesn't run from West to East
    # or from North to South.
    if orientation.lower() == "west-east":
        if first_x > last_x:
            logging.info("Flipping profile coords to get West-East orientation.")
            coords = coords[::-1]
    else: # orientation.lower() == "north-south":
        if first_y < last_y:
            logging.info("Flipping profile coords to get North-South orientation.")
            coords = coords[::-1]
    return lithogrid, coords
        
        
def euclidian_distance(x1, y1, x2, y2):
    return np.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)


def vertices_lengths(starts, ends):
    x1, y1 = starts.T
    x2, y2 = ends.T
    return euclidian_distance(x1, y1, x2, y2)


def find_vertex(s, s_start, s_end):
    return np.logical_and(s >= s_start, s <= s_end)


def point_on_vertex(s_alongline, vertex_coordinates):
    """Marks a point somewhere on a vertex of the profile"""
    starts = vertex_coordinates[:-1]
    ends = vertex_coordinates[1:]
    lengths = vertices_lengths(starts, ends)
    s_end = np.cumsum(lengths)
    s_start = np.insert(s_end[:-1], 0, 0.0)
    is_vertex = find_vertex(s_alongline, s_start, s_end)
    s_fromstart = s_alongline - s_start[is_vertex]
    x1, y1 = starts[is_vertex].T
    x2, y2 = ends[is_vertex].T
    dx = x2 - x1
    a = -np.arccos(dx / lengths[is_vertex])
    x = x1 + np.cos(a) * s_fromstart
    y = y1 + np.sin(a) * s_fromstart
    return x, y


def points_on_vertices(s_alongline, vertex_coordinates):
    """
    Vectorized form for speed increase
    
    See point_on_vertex() for unvectorized form.
    """
    starts = vertex_coordinates[:-1]
    ends = vertex_coordinates[1:]
    s_alongline = np.atleast_1d(s_alongline)
    starts = np.atleast_2d(starts)
    ends = np.atleast_2d(ends)

    nrow = len(s_alongline)
    ncol = len(starts)
    lengths = vertices_lengths(starts, ends)
    s_end = np.cumsum(lengths)
    s_start = np.insert(s_end[:-1], 0, 0.0)

    # broadcast
    starts = np.broadcast_to(array=starts.reshape(1, ncol, 2), shape=(nrow, ncol, 2))
    ends = np.broadcast_to(array=ends.reshape(1, ncol, 2), shape=(nrow, ncol, 2))
    lengths = np.broadcast_to(array=lengths.reshape(1, ncol), shape=(nrow, ncol))
    s_start = np.broadcast_to(array=s_start.reshape(1, ncol), shape=(nrow, ncol))
    s_end = np.broadcast_to(array=s_end.reshape(1, ncol), shape=(nrow, ncol))
    s = s_alongline.reshape(nrow, 1)

    # vectorized
    is_vertex = find_vertex(s, s_start, s_end)
    x1, y1 = starts[is_vertex].T
    x2, y2 = ends[is_vertex].T
    dx = x2 - x1
    a = -np.arccos(dx / lengths[is_vertex])
    s_fromstart = s_alongline - s_start[is_vertex]
    x = x1 + np.cos(a) * s_fromstart
    y = y1 + np.sin(a) * s_fromstart
    return x, y

# Colors ----------------------------------------------------------------------

def hexlist_to_rgbarray(hexlist):
    return (colour.notation.HEX_to_RGB(hexlist)[:, :3] * 255).astype(np.uint8)


def rgbarray_to_labarray(rgbarray):
    return skimage.color.rgb2lab(np.array([rgbarray]))[0]


def index_nearest_color(col, desired_colors, maxdist):
    # if distance is too large, kick 'm out?
    # should get rid of aliasing artefacts
    distances = abs(colour.delta_E(col, desired_colors, method="CIE 1976"))
    if distances.min() < maxdist:
        return np.argmin(distances)
    else:
        return None


def colormapping(image_rgb_colors, desired_rgb_colors, maxdist, linecolors):
    """
    Creates a list of tuples of (original_color, new_color), based on
    CIELAB distance between image colors, and desired colors.
    """
    image_labarray = rgbarray_to_labarray(image_rgb_colors / 255.0)
    desired_labarray = rgbarray_to_labarray(desired_rgb_colors / 255.0)

    mapping = []
    for rgb, lab in zip(image_rgb_colors, image_labarray):
        col_index = index_nearest_color(lab, desired_labarray, maxdist)
        if col_index is None:  # distance too large, should be replaced by white
            mapping.append((rgb, linecolors[0]))
        else:
            mapping.append((rgb, desired_rgb_colors[col_index]))
    return mapping


def replace_colors(image, mapping):
    """Replace rgb values in image as specified in mapping."""
    # avoid side effects
    im = image.copy()
    for color, replacement in mapping:
        im[(im == color).all(axis=-1)] = replacement
    return im


def coerce_colors(image, desired_hexlist, linecolors, maxdist=0.0):
    """ 
    Takes an `image` np.array (as provided `imageio`), and coerces the colors 
    to those defined in `desired_hexlist`, which is a list of colors in
    hexadecimal triplet representation (e.g #ffffff is white, rgb (255, 255, 255).
    
    A color in the image is replaced by the one in hexlist that is closest:
    which is shortest distance in CIELAB space.
    
    Distance function used is:
    https://colour.readthedocs.io/en/latest/generated/colour.delta_E.html#colour.delta_E
    """
    rgbarray = hexlist_to_rgbarray(desired_hexlist)
    image_rgbarray = np.unique(image.reshape(-1, image.shape[-1]), axis=0)
    linecolors = hexlist_to_rgbarray(linecolors)
    mapping = colormapping(image_rgbarray, rgbarray, maxdist, linecolors)
    return replace_colors(image, mapping)

# Last steps ------------------------------------------------------------------

def fill(data):
    """Basic N-dimensional nearest neighbour fill"""
    invalid = data == 255
    ind = ndimage.distance_transform_edt(
        invalid, return_distances=False, return_indices=True
    )
    return data[tuple(ind)]


def color_to_int_map(colors):
    hexlist = ["#" + k for k in colors.keys()]
    keys_rgb = hexlist_to_rgbarray(hexlist)
    mapping = []
    for i, (rgb, lithostring) in enumerate(zip(keys_rgb, colors.values())):
        if lithostring.lower() == "nodata":
            mapping.append((rgb, 0)) 
        elif lithostring.lower() == "linecolor":
            mapping.append((rgb, 255))
        else:
            mapping.append((rgb, i+1))
    return mapping


def int_to_litho_map(colors):
    mapping = {}
    for i, lithostring in enumerate(colors.values()):
        if lithostring.lower() == "nodata":
            pass     
        elif lithostring.lower() == "linecolor":
            pass
        else:
            mapping[i+1] = '"' + lithostring + '"'
    mapping[0] = np.nan
    return mapping


def image_to_lithogrid(image, mapping):
    nrow, ncol, _ = image.shape
    lithogrid = np.full((nrow, ncol), 0)
    for color, lithocode in mapping:
        lithogrid[(image == color).all(axis=-1)] = lithocode
    return lithogrid
    

def _compose_dataframe(name, tops, litho, x, y, idnum):
    df = pd.DataFrame()
    df["top"] = tops
    df["lithoclass"] = litho
    df["x"] = x
    df["y"] = y
    df["id"] = f"{name}/{idnum}"
    return df


def write_dlf(path, colors):
    hexlist = []
    litholist = []
    for hexstring, lithostring in colors.items():
        if lithostring.lower() != "nodata" and lithostring.lower() != "linecolor":
            litholist.append('"' + lithostring + '"') 
            hexlist.append(hexstring)
    r, g, b = hexlist_to_rgbarray(hexlist).T
    df = pd.DataFrame()
    df["class"] = litholist
    df["ired"] = r
    df["igreen"] = g
    df["iblue"] = b
    df["legend"] = litholist
    df.to_csv(path, index=False, quoting=csv.QUOTE_NONE)


# -----------------------------------------------------------------------------
    
def _main(path):
    logging.basicConfig(stream=sys.stdout, level=logging.INFO)
    config = read_toml(path, CONFIG_SCHEMA)
    name = config["name"]
    hexlist = ["#" + k for k in config["colors"].keys()]
    linecolors = ["#" + k for k, v in config["colors"].items() if v == "linecolor"]

    logging.info("Processing bitmap colors.")
    image = imageio.imread(config["bitmap"])
    processed_image = coerce_colors(image, hexlist, linecolors, maxdist=config["maximum_color_distance"])
    if config["pause"]:
        imageio.imwrite(f"tmp_{name}.bmp", processed_image)
    
    # prepare spatial data
    gdf = gpd.read_file(config["shapefile"])
    assert (
        len(gdf) == 1
    ), "Profile shapefile should contain only one LineString geometry."
    profile = gdf.geometry[0]
    assert (
        type(profile) == shapely.geometry.linestring.LineString
    ), "Profile must be a single LineString."

    # process image
    if config["pause"]:
        k = input("Finished editing temporary bitmap? Enter Q to quit and abort, press Enter to continue.")
        if k.lower() == "q":
            sys.exit()
        else:
            processed_image = imageio.imread(f"tmp_{name}.bmp")

    nrow, ncol, _ = processed_image.shape
    mapping = color_to_int_map(config["colors"])
    lithogrid = image_to_lithogrid(processed_image, mapping)
    
    logging.info("Generating spatial points.")
    coords = np.array(profile.coords)
    lithogrid, coords = _reorient(lithogrid, coords, config["orientation"])
    sampling_location = np.arange(0.0, profile.length, config["spacing"])
    xs, ys = points_on_vertices(sampling_location, coords)  

    zbot = config["elevation_lower"]
    ztop = config["elevation_upper"]
    dz = (ztop - zbot) / (nrow + 1)
    z = np.linspace(ztop, zbot + dz, nrow)
    dx = profile.length / ncol
    indices = (sampling_location / dx).astype(int)
    sample = fill(lithogrid)[:, indices]

    logging.info("Collecting lithological data.")
    dfs = []
    transitions = np.diff(sample, axis=0)
    transitions = np.insert(transitions, 0, 0, axis=0)
    for i, (lithocolumn, transition, x, y) in enumerate(zip(sample.T, transitions.T, xs, ys)):
        tops = z[transition != 0] 
        litho = lithocolumn[transition != 0]  
        dfs.append(_compose_dataframe(name, tops, litho, x, y, i+1))

    logging.info("Writing to IPF.")
    bigdf = pd.concat(dfs)
    lithostring_map = int_to_litho_map(config["colors"]) 
    bigdf["lithoclass"] = bigdf["lithoclass"].replace(lithostring_map)
    Path("ipf").mkdir(parents=True, exist_ok=True)
    imod.ipf.save(f"ipf/{name}.ipf", bigdf, itype="borehole1d")
    write_dlf(f"ipf/{name}.dlf", config["colors"])
    logging.info("All done.")


if __name__ == "__main__":
    path = sys.argv[1]
    _main(path)

Convert geological profiles into iMOD IPF borehole descriptions

Source code: https://gitlab.com/deltares/imod/profile2ipf

Introduction
============

Geohydrological layers are often generated from borehole data, using a variety
of interpolation schemes. However, in many cases the original borehole data is
no longer available, and only a drawn geological profile (cross-section) exists. 
This module aims at converting such a cross-section back into "borehole data".

Data Requirements
=================

The requirements for this module are:
* A shapefile describing the location of the cross-section along the profile
* A (cleaned-up) bitmap of the cross-section
* A configuration TOML specifying elevation, profile orientation, elevation,
  the mapping from color to lithology, etc.
  
Method
======

Firstly, the bitmap is reclassified: from colors to lithology. It does so based
on the translation table provided in the configuration file, where RGB 
hexadecimal values are mapped to lithological classes. These hex values can be 
attained easily from the bitmap with e.g. Inkscape's color picker. Some
allowance can be made for not exact color matches, by comparing distances in 
CIElab color space. The Euclidian distance is evaluated in CIElab rather than 
RGB space, because RGB space does not align well with human perception.

Secondly, a number of "borehole" locations are chosen, locations where the data of
the bitmap will be extracted. It does so by placing boreholes along the profile 
at a regular spacing. 

Thirdly, the bitmap is sampled at every borehole point, resulting in a single
column of pixels for every point. This collection of pixels is transformed into
a "borehole description" with tops and bottoms for every lithological layer,
by collecting the transitions from  one lithological class to another.

Finally, all these borehole descriptions are collected into a single `pandas`
DataFrame and written to IPF file format.
  
